# Python script: findAttachmentUsage
# ---------------------------------
# A Python script that reads a text file containing attachment file names,
# matches them against the source of Confluence pages,
# and produces a report on used and unused attachments.
# If you specify a page name on input, the script will append that page name to the search,
# thus checking only for references to files that are attached to the given page.
# See examples for expected format of input files.
#
# This script works with Python 3.2.3
#
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/manage-unused-attachments
# Example use case: http://ffeathers.wordpress.com/2013/06/02/how-to-manage-attachment-usage-in-confluence-wiki-with-some-python-scripts/

# Get from input: File containing attachment names, name of page containing attachments (optional), and directory containing content of pages

import json
import os

print("G'day! I'm the findAttachmentUsage Python script.\nGive me a list of attachment file names,\nplus a directory containing page source files,\nand I'll tell you which file names are referenced in the pages and which are not.\n")
print("If you give me a page name, I'll check only for references to the named attachments on that page.\n\n") 
input_filename = input("File containing attachment names (Example: 'attachments.txt'): ")
input_pagename = input("Optional: Page name, if you want to find usage of attachments attached to this page only (Example: 'my page name'): ")
input_directory = input("Directory containing content (Child of directory where script is. Example: 'pages'): ")

output_file_used_attachments = "used-attachments.txt"
output_file_unused_attachments = "unused-attachments.txt"

# Read the list of attachment file names and sizes, and store in attachments dictionary.
attachments_file = open(input_filename, "r")
attachments_dict = json.load(attachments_file)
attachments_file.close()

# Summary of purpose: Find the attachments that are used in one or more Confluence pages.
# First get each file in the page source directory.
attachments_matched_dict = {}
for item in os.listdir(input_directory):
  content_file = os.path.join(input_directory, item)
  if os.path.isdir(content_file):
    # Found a directory (unexpected)
    print("Skipping directory: " + content_file)
  else:
    # Found a file - get the page URL and page source
    contentf = open(content_file, "r")
    lines = contentf.readlines()
    if len(lines) > 1:
      page_url = lines[0]
      page_url = str.lstrip(page_url, "**")
      page_url = str.rstrip(page_url, "**\n")
      page_source = lines[1]
    contentf.close()
    # For each attachment in our dictionary,
    # search the page_source for a match on attachment name.
    # If found, add the attachment to a dictionary containing the matches.
    if len(lines) > 1:
      for attachment_name in attachments_dict.keys():
          # If the user gave us a page name, include it in the search string
          # else use just the attachment name in the search string
          if len(input_pagename) > 0:
              search_string = '<ri:attachment ri:filename="' + attachment_name + '"><ri:page ri:content-title="' + input_pagename
          else:
              search_string = attachment_name
          # Convert all to lower case, because comparison is case sensitive
          if str.lower(search_string) in str.lower(page_source):
              # If the attachment is already in the dictionary of matches, append the page URL to the dictionary entry
              # else add the attachment name and page URL to the dictionary
              if attachment_name in attachments_matched_dict:
                  attachments_matched_dict[attachment_name].append(page_url)
              else:
                  attachments_matched_dict[attachment_name] = [page_url]
                
# Write the dictionary of matched attachments to a file, in JSON format.
# Will overwrite existing files. 
used_attachments_file = open(output_file_used_attachments, "w+")
json.dump(attachments_matched_dict, used_attachments_file)
used_attachments_file.close()
      
# Now we've finished reading all the pages, and found all matched attachments.
# So we can now find attachments that are not used anywhere.
num_matched_attachments = 0
num_unmatched_attachments = 0
unmatched_file_size = 0

# Open the output file for writing. Will overwrite existing file.
unused_attachments_file = open(output_file_unused_attachments, "w+")

for attachment_name in attachments_dict.keys():
  if attachment_name in attachments_matched_dict:
    # Attachment is matched
    num_matched_attachments += 1
  else:
    # Attachment is not matched
    num_unmatched_attachments += 1
    unmatched_file_size += int(attachments_dict[attachment_name])
    unused_attachments_file.write(attachment_name + "\n")

unused_attachments_file.close()
print("\n\nAll done! I've put the results in these files: \nUsed attachments: " + output_file_used_attachments + "\nUnused attachments: " + output_file_unused_attachments)
print("Number of used attachments: " + str(num_matched_attachments))
print("Number of unused attachments: " + str(num_unmatched_attachments))
print("Sum of file sizes of unused attachments (in bytes): " + str(unmatched_file_size))
