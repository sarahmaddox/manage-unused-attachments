# Python script: getAttachmentCount
# ---------------------------------------
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/manage-unused-attachments
#
# A Python script that gets the number of attachments on each page in a given Confluence space.
# The script outputs a file containing the page URL and the number of attachments for each page,
# in descending sequence of number of attachments.
#
# This script works with Python 3.2.3

import xmlrpc.client

# Get from input: Confluence URL, username, password, space key

print("G'day! I'm the getAttachmentCount script.\nGive me a Confluence space, and I'll give you the number of attachments on each page in that space.\n")
print("Please take note: I look at PAGES only. Not blog posts.")
site_URL = input("Confluence site URL (exclude final slash): ")
username = input("Username: ")
pwd = input("Password: ")
spacekey = input("Space key: ")

num_pages = 0
pages_dict = {}

# Log in to Confluence
server = xmlrpc.client.ServerProxy(site_URL + "/rpc/xmlrpc")
token = server.confluence2.login(username, pwd)

# Get all the pages in the space
pages_list = server.confluence2.getPages(token, spacekey)

# For each page, count the attachments and add the page-URL and attachment-count to a dictionary
for page in pages_list:
    # Get the number of attachments on this page
    attachments_list = server.confluence2.getAttachments(token, page["id"])
    attachment_count = len(attachments_list)

    if attachment_count > 0:
        pages_dict[page["url"]] = attachment_count
        num_pages += 1

# Write the page URL and attachment-count to an output file.    
# Sort the output in descending order by attachment-count.

output_file_name = "attachment-count.txt"
output_file = open(output_file_name, "w+")

def SortByValue(dict_entry):
    return dict_entry[1]

for page_entry in sorted(pages_dict.items(), key=SortByValue, reverse=True):
    output_file.write(page_entry[0] + " : " + str(page_entry[1]) + "\n")

output_file.close()
print("\n\nAll done! I've put the results in this file: ", output_file_name)
print("Number of pages with attachments: " + str(num_pages))
