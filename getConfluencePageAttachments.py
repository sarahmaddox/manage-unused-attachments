# Python script: getConfluencePageAttachments
# ---------------------------------------
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/manage-unused-attachments
# Example use case: http://ffeathers.wordpress.com/2013/06/02/how-to-manage-attachment-usage-in-confluence-wiki-with-some-python-scripts/
#
# A Python script that gets all attachments on a given Confluence page.
# It puts the list of attachments into a text file, and prints a report of the number of attachments and total file size.
#
# Notes:
# * The script will ask you for a username and password. The given user must have permission to view the page.
#
# This script works with Python 3.2.3

import xmlrpc.client
import json

total_num_attachments = 0
total_file_size = 0

# Get from input: Confluence URL, username, password, space key, page name

print("G'day! I'm the getConfluencePageAttachments script.\nGive me a Confluence page, and I'll give you a list of all its attachments.\n")

site_URL = input("Confluence site URL (exclude final slash): ")
username = input("Username: ")
pwd = input("Password: ")
space_key = input("Space key: ")
page_name = input("Page name (example: my page title): ")
output_filename = "attachments.txt"

# Open the output file for writing. Will overwrite existing file.

output_file = open(output_filename, "w+")

# Log in to Confluence

server = xmlrpc.client.ServerProxy(site_URL + "/rpc/xmlrpc")
token = server.confluence2.login(username, pwd)

# Get the page

page = server.confluence2.getPage(token, space_key, page_name)

# Get the attachments on the page

attachments_list = server.confluence2.getAttachments(token, page["id"])

# For each attachment, get attachment file name and file size, and add them to a dictionary.
# Each dictionary entry contains the attachment file name and size.
# Also sum up the total number of attachments on the page and the sum or their file sizes.

attachments_dict={}

for attachment in attachments_list:
 attachments_dict[attachment["fileName"]] = attachment["fileSize"]
 total_num_attachments += 1
 total_file_size += int(attachment["fileSize"])

# Write the attachments dictionary out to a file, using JSON format
json.dump(attachments_dict, output_file)
output_file.close()
print("All done! I've put the results in this file: ", output_filename)
print("Total number of attachments: " + str(total_num_attachments) + "\nSum of file sizes (in bytes): " + str(total_file_size))
