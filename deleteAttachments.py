# Python script: deleteAttachments
# ---------------------------------
# A Python script that reads a text file containing attachment file names,
# accepts a Confluence page name,
# and removes the given attachments from the page.
#
# This script works with Python 3.2.3
#
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/manage-unused-attachments
# Example use case: http://ffeathers.wordpress.com/2013/06/02/how-to-manage-attachment-usage-in-confluence-wiki-with-some-python-scripts/

# Get from input: Input file name, and URL of Confluence page

import xmlrpc.client


def handle_error(error_attachment):
    print("Failed to delete attachment: " + error_attachment)

print("G'day! I'm the deleteAttachments Python script.\nGive me a list of file names, and a Confluence page,\nand I'll delete the given attachments from the page.\n")
site_URL = input("Confluence site URL (exclude final slash): ")
username = input("Username: ")
pwd = input("Password: ")
space_key = input("Space key: ")
page_name = input("Page name (example: my page title): ")
input_filename = input("File of attachment names (example: 'unused-attachments.txt'): ")

output_filename_deleted_attachments = "deleted-attachments.txt"

# Get the list of attachments to delete, from the input file.
attachments_file = open(input_filename, "r")

attachment_names = []

for attachment in attachments_file:
  # Add attachment name to a list, after stripping off the new-line character at the end
  attachment_names.append(str.rstrip(attachment, "\n"))

attachments_file.close()

# Log in to Confluence

server = xmlrpc.client.ServerProxy(site_URL + "/rpc/xmlrpc")
token = server.confluence2.login(username, pwd)

# Get the page

page = server.confluence2.getPage(token, space_key, page_name)
page_id = page["id"]

# Delete each attachment in the list

num_deleted_attachments = 0
num_failures = 0

deleted_attachments_file = open(output_filename_deleted_attachments, "w")

for attachment_name in attachment_names:
  try:
    if server.confluence2.removeAttachment(token, page_id, attachment_name):
      num_deleted_attachments += 1
      deleted_attachments_file.write(attachment_name + "\n")
    else:
      num_failures += 1
      handle_error(attachment_name)
  except:
    num_failures += 1
    handle_error(attachment_name)

deleted_attachments_file.close()

print("\n\nAll done! I've put the names of the deleted attachments in this file: " + output_filename_deleted_attachments)
print("Number of deleted attachments: " + str(num_deleted_attachments))
print("Number of failures: " + str(num_failures))
