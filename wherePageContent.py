# Python script: wherePageContent
# ---------------------------------
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/manage-unused-attachments
# Example use case: http://ffeathers.wordpress.com/2013/06/02/how-to-manage-attachment-usage-in-confluence-wiki-with-some-python-scripts/
#
print("G'day! I'm the wherePageContent script. I don't do much at all.")
print("I'm just here to tell you where to find the getConfluencePageContent script.")
print("It's in another repo:")
print("https://bitbucket.org/sarahmaddox/confluence-full-text-search/src")
print("Bye!")
